![Alt text](http://fusengine.ch/img/deluge.svg)
=============================================
# Delug-web torrent

#### Directory and port

```
 - Volume:  /data            
 - Volume: /root/Downloads   

 - Port:  8112               
 - Port:  53160              
 - Port:  53160/udp          
 - Port:  5884               

```

#### Default

- Password = deluge
- Download = /root/Downloads
- Web-ui   = 8112 ->   http://mydomaine.me:8112


&copy; 2017[Fusengine](http://fusengine.com)